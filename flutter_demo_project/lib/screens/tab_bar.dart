import 'package:flutter/material.dart';

class CustomTabBar extends StatefulWidget {
  final List<String> tabs;
  final Function(int) onTap;
  final int initialIndex;
  final Color backgroundColor;
  final Color selectedColor;
  final Color unselectedColor;

  CustomTabBar({
    required this.tabs,
    required this.onTap,
    this.initialIndex = 0,
    this.backgroundColor = Colors.white,
    this.selectedColor = Colors.blue,
    this.unselectedColor = Colors.grey,
  });

  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: widget.tabs.length,
      vsync: this,
      initialIndex: widget.initialIndex,
    );
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        widget.onTap(_tabController.index);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backgroundColor,
      child: TabBar(
        controller: _tabController,
        labelColor: widget.selectedColor,
        unselectedLabelColor: widget.unselectedColor,
        tabs: List.generate(
          widget.tabs.length,
          (index) => Tab(text: widget.tabs[index]),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
