import 'package:flutter/material.dart'
    show
        Axis,
        BuildContext,
        Colors,
        Container,
        EdgeInsets,
        ListView,
        SizedBox,
        StatelessWidget,
        Widget,
        runApp;
import '../widgets/card/card.dart';

void main() => runApp(const LightBlueBox());

class LightBlueBox extends StatelessWidget {
  const LightBlueBox({super.key});

  @override
  Widget build(BuildContext context) {
    Widget horizontalList = Container(
        margin: const EdgeInsets.only(left: 0.0),
        // height: 300.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: const <Widget>[
            SizedBox(width: 50),
            CardView(),
            SizedBox(width: 60),
            CardView(),
            SizedBox(width: 60),
            CardView(),
            SizedBox(width: 60),
            CardView(),
            SizedBox(width: 60),
          ],
        ));
    return Container(
      color: Colors.transparent,
      height: 150.0,
      child: horizontalList,
    );
  }
}
