import 'package:flutter/material.dart';

void main() => runApp(const CourseDetail());

class CourseDetail extends StatelessWidget {
  const CourseDetail({super.key});

  @override
  Widget build(BuildContext context) {
    print('shubham10');
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(
            bottom: 390,

            // ignore: sized_box_for_whitespace
            child: Container(
              width: 400,
              height: 420,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: const DecorationImage(
                  image: AssetImage("images/webInterFace.png"),
                  fit: BoxFit.cover,
                ),
              ),
            )),
        Container(
            width: 500,
            height: 500,
            padding: const EdgeInsets.only(top: 10),
            margin: const EdgeInsets.only(top: 420),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(50), topLeft: Radius.circular(50)),
              color: Color.fromARGB(255, 241, 241, 241),
            ),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 0, top: 15, right: 150),
                  child: const Text(
                    'Web design cource',
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 20, top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 100),
                          child: const Text(
                            '\$25,99',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.lightBlue,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        const SizedBox(width: 20),
                        Container(
                          margin: const EdgeInsets.only(right: 50),
                          child: Row(
                            children: const [
                              Text(
                                '4.3',
                                style: TextStyle(fontSize: 15),
                              ),
                              SizedBox(width: 4),
                              Icon(
                                Icons.star,
                                color: Colors.lightBlue,
                                size: 15.0,
                                textDirection: TextDirection.ltr,
                                semanticLabel: 'Icon',
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(left: 20, top: 30),
                        child: Row(
                          children: [
                            Container(
                                height: 50,
                                width: 65,
                                margin: const EdgeInsets.only(top: 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Container(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Column(
                                    children: const [
                                      Text(
                                        '24',
                                        style: TextStyle(
                                            color: Colors.lightBlue,
                                            fontSize: 15),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        'Classe',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 14),
                                      ),
                                    ],
                                  ),
                                )),
                            const SizedBox(width: 20),
                            Container(
                              height: 50,
                              width: 65,
                              margin: const EdgeInsets.only(top: 5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                padding: const EdgeInsets.only(top: 10),
                                child: Column(
                                  children: const [
                                    Text(
                                      '2hour',
                                      style: TextStyle(
                                          color: Colors.lightBlue,
                                          fontSize: 15),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Time',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(width: 20),
                            Container(
                              height: 50,
                              width: 65,
                              margin: const EdgeInsets.only(top: 5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                padding: const EdgeInsets.only(top: 10),
                                child: Column(
                                  children: const [
                                    Text(
                                      '2hour',
                                      style: TextStyle(
                                          color: Colors.lightBlue,
                                          fontSize: 15),
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                      'Seat',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )),
                  ],
                ),
                const SizedBox(height: 40),
                const Padding(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  child: Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elitsed afsf sdf gsdf tempor incididunt ut labore et dolore magna aliqua.",
                    style: TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(191, 66, 64, 64),
                        fontWeight: FontWeight.w500),
                  ),
                ),
                const SizedBox(height: 60),
                Row(
                  children: [
                    // ignore: avoid_unnecessary_containers
                    Container(
                      width: 100,
                      padding: const EdgeInsets.only(left: 15),
                      decoration: const BoxDecoration(
                          // color: Colors.red,
                          ),
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(
                            color: Colors.transparent,
                          ),
                        ),
                        onPressed: () => {Navigator.pop(context)},
                        child: Container(
                          width: 50,
                          height: 45,
                          padding: const EdgeInsets.only(top: 10, left: 18),
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.circular(10)),
                          // margin: const EdgeInsets.only(left: 30),
                          child: const Text(
                            'X',
                            style: TextStyle(color: Colors.grey, fontSize: 20),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 40,
                      width: 230,
                      decoration: BoxDecoration(
                          color: Colors.lightBlue,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Center(
                        child: Text(
                          'Join Cources',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                )
              ],
            )),
        Positioned(
            bottom: 380,
            right: 50,

            // ignore: sized_box_for_whitespace
            child: Container(
                width: 50,
                height: 50,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.lightBlue,
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: const Icon(
                    Icons.favorite,
                    color: Colors.white,
                    size: 24.0,
                    semanticLabel: 'Text to announce in accessibility modes',
                  ),
                ))),
      ],
    );
  }
}
