import 'package:flutter/material.dart'
    show
        BuildContext,
        Container,
        EdgeInsets,
        GridView,
        StatelessWidget,
        Widget,
        runApp;
import '../widgets/card/gridCard.dart';

void main() => runApp(const GridBox());

class GridBox extends StatelessWidget {
  const GridBox({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 0.0),
        height: 500.0,
        width: 350,
        child: GridView.count(
          mainAxisSpacing: 50,
          crossAxisSpacing: 10,
          primary: false,
          padding: const EdgeInsets.all(0.0),
          crossAxisCount: 2,
          children: const [
            GridCardView(),
            GridCardView(),
            GridCardView(),
            GridCardView()
          ],
        ));
  }
}
