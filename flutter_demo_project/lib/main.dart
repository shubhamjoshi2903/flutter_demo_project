import 'package:flutter/material.dart';
import 'screens/color_block.dart';
import 'screens/grid.dart';
import 'screens/details.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      _currentIndex = _tabController.index;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget navbarSection = Container(
        height: 200,
        margin:
            const EdgeInsets.only(left: 20.0, right: 20.0, top: 50, bottom: 20),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.all(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: const <Widget>[
                      Text(
                        'Choose your       ',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(height: 6),
                      Text(
                        'Design Course',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                      )
                    ],
                  ),
                  Image.asset(
                    ('images/userImage.png'),
                    height: 40,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 35),
            Column(children: [
              Container(
                width: 300,
                height: 46,
                // color: Colors.red,
                margin: const EdgeInsets.only(right: 20),
                padding: const EdgeInsets.only(
                  top: 5,
                  left: 3,
                ),
                alignment: Alignment.topLeft,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(6.0),
                        bottomRight: Radius.circular(6.0),
                        topLeft: Radius.circular(6.0),
                        bottomLeft: Radius.circular(6.0)),
                    color: Color.fromARGB(255, 243, 241, 241)),
                child: const TextField(
                  decoration: InputDecoration(
                    hintText: 'Search your course',
                    contentPadding: EdgeInsets.only(left: 5),
                    hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                    border: InputBorder.none,
                    suffixIcon: Icon(
                      Icons.search_rounded,
                      color: Colors.black,
                      size: 25.0,
                    ),
                  ),
                ),
              ),
            ]),
            const SizedBox(
              height: 25,
            ),
            // ignore: avoid_unnecessary_containers
            Container(
              alignment: Alignment.topLeft,
              // padding: const EdgeInsets.only(left: 1),
              child: const Text(
                'Category',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        ));

    return MaterialApp(
      home: Scaffold(
        extendBody: true,
        appBar: AppBar(
          elevation: 0,
          toolbarHeight: 250,
          title: navbarSection,
          backgroundColor: Colors.transparent,
          bottom: TabBar(
            padding: const EdgeInsets.only(left: 30, right: 30),
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(
                25.0,
              ),
              color: Colors.blue,
            ),
            controller: _tabController,
            indicatorColor: Colors.blue,
            labelColor: Colors.white,
            unselectedLabelColor: Colors.blue,
            tabs: [
              Tab(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(color: Colors.blue, width: 1)),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text("UI/UX"),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(color: Colors.blue, width: 1)),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text("Coding"),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(color: Colors.blue, width: 1)),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text("Basic UI"),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            SingleChildScrollView(
              padding: const EdgeInsets.only(top: 50),
              child: Column(children: [
                SizedBox(
                    height: 150,
                    child: ListView(
                      children: const [
                        SizedBox(height: 0),
                        LightBlueBox(),
                      ],
                    )),
                const SizedBox(height: 30),
                Container(
                    padding: const EdgeInsets.only(right: 180),
                    child: const Text(
                      'Popular Course   ',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    )),
                const SizedBox(
                  height: 10,
                ),
                const GridBox(),
              ]),
            ),
            const Center(
              child: Text('Tab 2 selected'),
            ),
            const GridBox(),
          ],
        ),
      ),
    );
  }
}
