import 'package:flutter/material.dart';
import '../../screens/details.dart';

void main() => runApp(const CardView());

class CardView extends StatelessWidget {
  const CardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: const BorderSide(
              color: Colors.transparent,
            ),
          ),
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => Scaffold(
                        extendBody: true,
                        body: Container(
                          decoration: const BoxDecoration(color: Colors.white),
                          child: const CourseDetail(),
                        ),
                      )),
            )
          },
          child: Container(
              width: 220,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: const Color.fromARGB(255, 241, 241, 241),
              ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 80, top: 10),
                    child: const Text(
                      'User Interface Design  ',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 82, top: 20),
                      child: Row(
                        children: [
                          const Text(
                            '24 lesson',
                            style: TextStyle(
                                fontSize: 16,
                                color: Color.fromARGB(191, 66, 64, 64)),
                          ),
                          const SizedBox(width: 20),
                          Row(
                            children: const [
                              Text(
                                '4.3',
                                style: TextStyle(fontSize: 15),
                              ),
                              SizedBox(width: 4),
                              Icon(
                                Icons.star,
                                color: Colors.lightBlue,
                                size: 15.0,
                                textDirection: TextDirection.ltr,
                                semanticLabel: 'Icon',
                              ),
                            ],
                          ),
                        ],
                      )),
                  Row(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(left: 82, top: 10),
                          child: Row(
                            children: [
                              const Text(
                                '\$25',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.lightBlue,
                                    fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(width: 56),
                              Container(
                                height: 35,
                                width: 35,
                                margin: const EdgeInsets.only(top: 5),
                                decoration: BoxDecoration(
                                  color: Colors.lightBlue,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: const Center(
                                  child: Text(
                                    '+',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 25),
                                  ),
                                ),
                              )
                            ],
                          )),
                    ],
                  )
                ],
              )),
        ),
        Positioned(
            top: 30,
            right: 180,
            child: Container(
              width: 100,
              height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: const DecorationImage(
                  image: AssetImage("images/interFace1.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ))
      ],
    );
  }
}
