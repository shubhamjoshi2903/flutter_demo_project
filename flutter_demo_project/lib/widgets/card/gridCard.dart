import 'package:flutter/material.dart';

void main() => runApp(const GridCardView());

class GridCardView extends StatelessWidget {
  const GridCardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
            width: 200,
            height: 220,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: const Color.fromARGB(255, 241, 241, 241),
            ),
            child: Column(
              children: [
                Container(
                  constraints: const BoxConstraints(maxWidth: 150),
                  padding: const EdgeInsets.only(top: 10),
                  child: const Text(
                    'App design course',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 18, top: 20),
                    child: Row(
                      children: [
                        const Text(
                          '24 lesson',
                          style: TextStyle(
                              fontSize: 16,
                              color: Color.fromARGB(191, 66, 64, 64)),
                        ),
                        const SizedBox(width: 20),
                        Row(
                          children: const [
                            Text(
                              '4.3',
                              style: TextStyle(fontSize: 15),
                            ),
                            SizedBox(width: 4),
                            Icon(
                              Icons.star,
                              color: Colors.lightBlue,
                              size: 15.0,
                              textDirection: TextDirection.ltr,
                              semanticLabel: 'Icon',
                            ),
                          ],
                        ),
                      ],
                    )),
              ],
            )),
        Positioned(
            top: 110,
            right: 20,
            child: Container(
              width: 130,
              height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: const DecorationImage(
                  image: AssetImage("images/interFace3.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ))
      ],
    );
  }
}
